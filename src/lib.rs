//! A simple library to wrap an existing storage with history-keeping functionality. Any changes to
//! the underlying component are store to be retrieved at a later time.
//!
//! ```rust
//! # use specs::hibitset::BitSetLike;
//! # use specs::storage::{MaskedStorage, Storage, TryDefault, UnprotectedStorage, VecStorage};
//! # use specs::world::{Component, Entity, Index};
//! # use std::marker::PhantomData;
//! # use std::ops::Deref;
//! # use specs::storage::{ReadStorage, WriteStorage};
//! # use specs::{Builder, World, WorldExt};
//! # use specs_history::{StorageExt, HistoricStorage};
//! #
//! #[derive(Clone, PartialEq, Debug)]
//! struct Pos(usize);
//! impl Component for Pos {
//!     // Using `Vec` as the history storage causes the program to store all changes (no maximum
//!     // size).
//!     type Storage = HistoricStorage<Self, VecStorage<Self>, VecStorage<Vec<Self>>, Vec<Self>>;
//! }
//!
//! #[derive(Clone, PartialEq, Debug)]
//! struct Vel(usize);
//! impl Component for Vel {
//!     // Using `arraydeque::ArrayDeque` as the history storage causes the program to store a
//!     // limited amount of history (e.g. 2 changes in this chase).
//!     type Storage = HistoricStorage<
//!         Self,
//!         VecStorage<Self>,
//!         VecStorage<arraydeque::ArrayDeque<[Self; 2], arraydeque::behavior::Wrapping>>,
//!         arraydeque::ArrayDeque<[Self; 2], arraydeque::behavior::Wrapping>
//!     >;
//! }
//!
//! # fn main() {
//! let mut world = World::new();
//!
//! world.register::<Pos>();
//! world.register::<Vel>();
//!
//! let a = world.create_entity().with(Pos(1)).build();
//!
//! // Any changes here...
//! world.exec(|mut pos: WriteStorage<Pos>| {
//!     pos.get_mut(a).unwrap().0 = 3;
//!     pos.insert(a, Pos(2));
//!     pos.get_mut(a).unwrap().0 = 7;
//!     pos.insert(a, Pos(4));
//! });
//!
//! // Are retrivable here...
//! world.exec(|pos: ReadStorage<Pos>| {
//!     assert_eq!(
//!         &[Pos(1), Pos(3), Pos(2), Pos(7)][..],
//!         pos.history(a).unwrap()
//!     );
//! });
//!
//! let b = world.create_entity().with(Vel(1)).build();
//!
//! // Any changes here...
//! world.exec(|mut vel: WriteStorage<Vel>| {
//!     vel.get_mut(b).unwrap().0 = 3;
//!     vel.insert(b, Vel(2));
//!     vel.get_mut(b).unwrap().0 = 7;
//!     vel.insert(b, Vel(4));
//! });
//!
//! // Are retrivable here... (but only the latest 2 changes)
//! world.exec(|vel: ReadStorage<Vel>| {
//!     assert_eq!(
//!         &[Vel(2), Vel(7)][..],
//!         &*vel.history(b).unwrap().cloned().collect::<Vec<_>>()
//!     );
//! });
//! # }
//! ```

use specs::hibitset::BitSetLike;
use specs::storage::{MaskedStorage, Storage, TryDefault, UnprotectedStorage};
use specs::world::{Component, Entity, Index};
use std::marker::PhantomData;
use std::ops::Deref;

pub trait Archiver<'s, C: 's> {
    type Archive: 's;

    fn archive(&mut self, value: C);
    fn view(&'s self) -> Self::Archive;
}

impl<'s, C: 's> Archiver<'s, C> for Vec<C> {
    type Archive = &'s [C];

    fn archive(&mut self, value: C) {
        self.push(value);
    }

    fn view(&'s self) -> Self::Archive {
        &*self
    }
}

#[cfg(feature = "arraydeque")]
impl<'s, C: 's, A: arraydeque::Array<Item = C>> Archiver<'s, C>
    for arraydeque::ArrayDeque<A, arraydeque::behavior::Wrapping>
{
    type Archive = arraydeque::Iter<'s, C>;

    fn archive(&mut self, value: C) {
        self.push_back(value);
    }

    fn view(&'s self) -> Self::Archive {
        self.iter()
    }
}

/// A trait implemented on storages to maintain a history of changes.
pub trait Historic<'h, C: 'h>: UnprotectedStorage<C> {
    type History: 'h;

    /// # Safety
    ///
    /// The given `id` must have already been inserted into the storage exactly once, and it must
    /// not have been removed.
    unsafe fn history(&'h self, id: Index) -> Self::History;
}

/// A storage type which keeps a vec of changes for each component.
pub struct HistoricStorage<C, T, H, HH> {
    // Store the history of changes for each index.
    history: H,
    history_storage: PhantomData<HH>,
    // The underlying storage which keeps the current/active state.
    storage: T,
    phantom: PhantomData<C>,
}

impl<C, T, H, HH> std::default::Default for HistoricStorage<C, T, H, HH>
where
    T: TryDefault,
    H: TryDefault,
{
    fn default() -> Self {
        HistoricStorage {
            history: H::unwrap_default(),
            history_storage: PhantomData,
            storage: T::unwrap_default(),
            phantom: PhantomData,
        }
    }
}

impl<'s, C, T, H, HH> UnprotectedStorage<C> for HistoricStorage<C, T, H, HH>
where
    C: Component + Clone,
    T: UnprotectedStorage<C>,
    H: UnprotectedStorage<HH>,
    HH: Archiver<'s, C> + Default,
{
    unsafe fn clean<B>(&mut self, has: B)
    where
        B: BitSetLike,
    {
        self.history.clean(&has);
        self.storage.clean(&has);
    }

    unsafe fn get(&self, id: Index) -> &C {
        self.storage.get(id)
    }

    unsafe fn get_mut(&mut self, id: Index) -> &mut C {
        let old_comp = self.get(id).clone();
        self.history.get_mut(id).archive(old_comp);
        self.storage.get_mut(id)
    }

    unsafe fn insert(&mut self, id: Index, comp: C) {
        self.history.insert(id, Default::default());
        self.storage.insert(id, comp);
    }

    unsafe fn remove(&mut self, id: Index) -> C {
        self.history.remove(id);
        self.storage.remove(id)
    }
}

impl<'h, C, T, H, HH> Historic<'h, C> for HistoricStorage<C, T, H, HH>
where
    C: Component + Clone + 'h,
    T: UnprotectedStorage<C>,
    H: UnprotectedStorage<HH>,
    HH: Archiver<'h, C> + Default,
{
    type History = HH::Archive;

    unsafe fn history(&'h self, id: Index) -> Self::History {
        // SAFETY: This is safe due to the invariants for calling `history`.
        self.history.get(id).view()
    }
}

pub trait StorageExt<'h, C: 'h> {
    type History: 'h;
    /// Returns the history of changes for the component for a given entity.
    fn history(&'h self, entity: Entity) -> Option<Self::History>;
}

impl<'h, C: 'h, D> StorageExt<'h, C> for Storage<'h, C, D>
where
    C: Component + Clone,
    C::Storage: Historic<'h, C>,
    D: Deref<Target = MaskedStorage<C>>,
{
    type History = <C::Storage as Historic<'h, C>>::History;

    fn history(&'h self, entity: Entity) -> Option<Self::History> {
        if self.contains(entity) {
            // SAFETY: We checked the mask, so all invariants are met.
            Some(unsafe { self.unprotected_storage().history(entity.id()) })
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use specs::storage::{ReadStorage, VecStorage, WriteStorage};
    use specs::{Builder, World, WorldExt};

    #[derive(Clone, PartialEq, Debug)]
    struct Pos(usize);

    impl Component for Pos {
        type Storage = HistoricStorage<Self, VecStorage<Self>, VecStorage<Vec<Self>>, Vec<Self>>;
    }

    #[test]
    fn simple() {
        let mut world = World::new();

        world.register::<Pos>();

        let a = world.create_entity().with(Pos(1)).build();

        // Any changes here...
        world.exec(|mut pos: WriteStorage<Pos>| {
            pos.get_mut(a).unwrap().0 = 3;
            pos.insert(a, Pos(2)).unwrap();
            pos.get_mut(a).unwrap().0 = 7;
            pos.insert(a, Pos(4)).unwrap();
        });

        // Are retrivable here...
        world.exec(|pos: WriteStorage<Pos>| {
            assert_eq!(
                &[Pos(1), Pos(3), Pos(2), Pos(7)][..],
                pos.history(a).unwrap()
            );
        });
    }

    #[test]
    fn limited_history() {
        #[derive(Clone, PartialEq, Debug)]
        struct Vel(usize);
        impl Component for Vel {
            type Storage = HistoricStorage<
                Self,
                VecStorage<Self>,
                VecStorage<arraydeque::ArrayDeque<[Self; 2], arraydeque::behavior::Wrapping>>,
                arraydeque::ArrayDeque<[Self; 2], arraydeque::behavior::Wrapping>,
            >;
        }

        let mut world = World::new();

        world.register::<Vel>();

        let b = world.create_entity().with(Vel(1)).build();

        // Any changes here...
        world.exec(|mut vel: WriteStorage<Vel>| {
            vel.get_mut(b).unwrap().0 = 3;
            vel.insert(b, Vel(2)).unwrap();
            vel.get_mut(b).unwrap().0 = 7;
            vel.insert(b, Vel(4)).unwrap();
        });

        // Are retrivable here... (but only the latest 2 changes)
        world.exec(|vel: ReadStorage<Vel>| {
            assert_eq!(
                &[Vel(2), Vel(7)][..],
                &*vel.history(b).unwrap().cloned().collect::<Vec<_>>()
            );
        });
    }
}
